import configparser
import json
from pprint import pformat
import os
import stat
import time

import yaml
import boto3
from bitbucket_pipes_toolkit import Pipe, get_logger
from botocore.exceptions import ClientError, ParamValidationError, WaiterError

logger = get_logger()

schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "CLUSTER_NAME": {
        "type": "string",
        "required": True
    },
    "SERVICE_NAME": {
        "type": "string",
        "required": True
    },
    "TASK_DEFINITION": {
        "type": "string",
        "required": False
    },
    "FORCE_NEW_DEPLOYMENT": {
        "type": "boolean",
        "default": False
    },
    "WAIT": {
        "type": "boolean",
        "default": False
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class ECSDeploy(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, pipe_metadata=None, schema=None, env=None, client=None, check_for_newer_version=False):
        self.auth_method = self.discover_auth_method()
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env,
                         check_for_newer_version=check_for_newer_version)
        self.client = client
        self.action = None

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                os.environ.pop('AWS_ACCESS_KEY_ID', None)
                os.environ.pop('AWS_SECRET_ACCESS_KEY', None)

                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(f'{aws_config_directory}/.aws-oidc', f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))
            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)

            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)

            logger.debug('Configured settings for authentication with assume web identity role')

    def get_client(self):
        try:
            return boto3.client('ecs', region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as err:
            self.fail("Failed to create boto3 client.\n" + str(err))

    def _handle_update_service_error(self, error):
        error_code = error.response['Error']['Code']
        if error_code == 'ClusterNotFoundException':
            msg = f'ECS cluster not found. Check your CLUSTER_NAME.'
        elif error_code == 'ServiceNotFoundException':
            msg = f'ECS service not found. Check your SERVICE_NAME'
        else:
            msg = f"Failed to update the stack.\n" + str(error)
        self.fail(msg)

    def update_task_definition(self, task_definition_file, image=None):

        logger.info(f'Updating the task definition...')

        client = self.get_client()
        try:
            with open(task_definition_file) as d_file:
                task_definition = json.load(d_file)
        except json.decoder.JSONDecodeError:
            self.fail('Failed to parse the task definition file: invalid JSON provided.')
        except FileNotFoundError:
            self.fail(f'Not able to find {task_definition_file} in your repository.')

        logger.info(f'Using task definition: \n{pformat(task_definition)}')

        try:
            response = client.register_task_definition(**task_definition)
            return response['taskDefinition']['taskDefinitionArn']
        except ClientError as err:
            self.fail("Failed to update the stack.\n" + str(err))
        except KeyError as err:
            self.fail("Unable to retrieve taskDefinitionArn key.\n" + str(err))
        except ParamValidationError as err:
            self.fail(f"ECS task definition parameter validation error: \n {err.args[0]}")

    def list_task_definitions(self):

        logger.info(f'Check for active task definition...')

        client = self.get_client()

        try:
            response = client.list_task_definitions()
            return response.get('taskDefinitionArns')
        except ClientError as err:
            self.fail("Failed to fetch list of task definitions.\n" + str(err))

    def update_service(self, cluster, service, task_definition=None, force_new_deployment=False):

        logger.info(f'Update the {service} service.')

        client = self.get_client()

        parameters = {
            "cluster": cluster,
            "service": service,
            "taskDefinition": task_definition,
            "forceNewDeployment": force_new_deployment,
        }

        if task_definition is None:
            parameters.pop('taskDefinition')

        try:
            response = client.update_service(**parameters)
            return response
        except ClientError as err:
            self._handle_update_service_error(err)

    def run(self):
        super().run()
        self.enable_debug_log_level()
        self.auth()
        region = self.get_variable('AWS_DEFAULT_REGION')
        definition = self.get_variable('TASK_DEFINITION')
        try:
            image = self.get_variable('IMAGE_NAME')
        except KeyError:
            image = None
        cluster_name = self.get_variable('CLUSTER_NAME')
        service_name = self.get_variable('SERVICE_NAME')
        force_new_deployment = self.get_variable('FORCE_NEW_DEPLOYMENT')

        if definition is None:
            task_definition = None
            # try to check any active task_definitions on the service
            if not self.list_task_definitions():
                self.fail(
                    f'There are no active tasks defifnitions for the service {service_name} on the cluster {cluster_name}. '
                    f'Check your service on the AWS Console or provide the TASK_DEFINITION variable.'
                )
        else:
            task_definition = self.update_task_definition(definition, image)

        response = self.update_service(
            cluster_name, service_name, task_definition, force_new_deployment)

        logger.debug(response)

        if self.get_variable('WAIT'):
            logger.info('Waiting for service to become Stable...')

            client = self.get_client()
            waiter = client.get_waiter('services_stable')
            try:
                waiter.wait(cluster=cluster_name, services=[service_name])
            except WaiterError as e:
                self.fail(f'Error waiting for service to become stable: {e}')
            self.log_info(f'Service {service_name} has become stable')

        self.success(f'Successfully updated the {service_name} service. You can check you service here: \n'
                     f'https://console.aws.amazon.com/ecs/home?region={region}#/clusters/{cluster_name}/services/{service_name}/details')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = ECSDeploy(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
